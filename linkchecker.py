import os
import requests
from bs4 import BeautifulSoup
import argparse



parser = argparse.ArgumentParser(description='Check a web page for broken and HTTPS upgradeable links')
parser.add_argument('uri', help='the folder to process')
parser.add_argument('-v', '--verbose', action='store_true', help='show verbose output')

args = parser.parse_args()

uri = args.uri
show_output = args.verbose


try:
    response = requests.get(uri)

    if not response:
        print('Page not accessible, aborting')
    else:
        print('Page accessible. This may take some time....')
        print('')

        broken_links = []
        upgradeable_links = []

        soup = BeautifulSoup(response.text, 'html.parser')
        links = soup.find_all('a', href=True)

        broken_links = []
        upgradeable_links = []

        for link_tag in links:
            href = link_tag.get('href')
            link_text = link_tag.get_text().strip()
            
            display_text = link_text

            if link_text is '':
                link_text = href

            # Check absolute links for accessibility and upgradeability
            if href.startswith('http'):
                if show_output:
                    print(f'Evaluating link: {link_text}')

                try:
                    link_test = requests.get(href)
                except requests.exceptions.RequestException:
                    link_test = False

                if not link_test:
                    broken_links.append(href)

                    if show_output:
                        print('    Link broken')
                else:
                    if href.startswith('http://'):
                        upgraded_href = href.replace('http:', 'https:', 1)

                        try:
                            upgraded_test = requests.get(upgraded_href)

                            if upgraded_test:
                                upgradeable_links.append(href)

                                if show_output:
                                    print('    Link upgradeable to HTTPS')

                        except requests.exceptions.RequestException:
                            #Do nothing because it isn't upgradeable
                            pass
            else:
                if show_output:
                    print(f'Skipping link with missing HTTP/S schema: {link_text}')

            if show_output:
                print('')

        if len(broken_links) > 0 or len(upgradeable_links) > 0:
            if show_output:
                print('')
                print('')
                print('')
                print('')

            if len(broken_links) > 0:
                print('Broken Links:')

                for broken_link in broken_links:
                    print(broken_link)

                if show_output and len(upgradeable_links) > 0:
                    print('')
            if len(upgradeable_links) > 0:
                print('Upgradeable Links:')

                for upgradeable_link in upgradeable_links:
                    print(upgradeable_link)
        else:
            print('No issues found')                

        print('')

except:
    print('Please provide a proper wepbage URl with schema')