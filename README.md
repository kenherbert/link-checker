A scraper to check a web page for broken links and links that can be upgraded to HTTPS.


## Usage

Just run `python linkchecker.py`.


## Command line options
`-v` or `--verbose` - Show verbose output
